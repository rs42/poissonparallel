#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>
#include <string>
#include <chrono>
using namespace std;


inline double u_exact(const double & x, const double & y)
{
    /*!
     * \brief Exact solution of Poisson equation - \Delta u + q u = F
     *        on a 2D rectange with third type boundary conditions
     */
    return sqrt(4 + x * y);
}
inline double psi_L(const double & x, const double & y)
{
   // assert(fabs(x) < 1e-8);
    const double t = sqrt(4 + x * y);
    return - y/t/2 + t;
}
inline double psi_R(const double & x, const double & y)
{
   // assert(fabs(x - 4) < 1e-8);
    const double t = sqrt(4 + x * y);
    return  y/t/2 + t;
}
inline double psi_B(const double & x, const double & y)
{
   // assert(fabs(y) < 1e-8);
    const double t = sqrt(4 + x * y);
    return - x/t/2 + t;
}
inline double psi_T(const double & x, const double & y)
{
   // assert(fabs(y - 3) < 1e-8 );
    const double t = sqrt(4 + x * y);
    return  x/t/2 + t;
}
inline double F(const double &x, const double &y)
{
    return sqrt(4 + x * y) * ( (x*x + y*y) * pow(4 + x * y, -2)/4  + x + y);
}
inline double q(const double &x, const double &y)
{
    return x + y;
}
#if 0
double dotProduct(const double * u, const double * v, const int & M, const int & N, const double & h1, const double & h2)
{//this must be fast
    double res = 0;
    for (int j = M; j < M * (N - 1); j += M) {
        for (int i = 1; i < M - 1; i++) {
            const int idx = i + j;
            res += u[idx] * v[idx];
        }
    }

    double t1 = 0;
    for (int i = 0; i < M; i++)
        t1 += u[i] * v[i];

    for (int i = 0; i < M; i++) {
        const int idx = i + M * (N - 1);
        t1 += u[idx] * v[idx];
    }

    for (int j = 1; j < N - 1; j++) {
        const int idx1 = M * j, idx2 = idx1 + M - 1;
        t1 += u[idx1] * v[idx1] + u[idx2] * v[idx2];
    }
    res += t1/2;
    return res * h1 * h2;
}
#else
double dotProduct(const double * u, const double * v, const int & M, const int & N, const double & h1, const double & h2)
{//this must be fast
    double res = u[0] * v[0];
    for (int i = 1; i < M - 1; i++)
        res += u[i] * v[i];
    res += u[M-1] * v[M-1];
    res /= 2;
    for (int j = 1; j < N - 1; j++) {
        res += u[j*M] * v[j*M] / 2;
        for (int i = 1; i < M - 1; i++) {
            res += u[i+j*M] * v[i+j*M];
        }
        res += u[M-1+j*M] * v[M-1+j*M] / 2;
    }
    res += u[(N-1)*M] * v[(N-1)*M] / 2;
    for (int i = 1; i < M - 1; i++) {
        res += u[i + (N-1)*M] * v[i + (N-1)*M] / 2;
    }
    res += u[N*M - 1] * v[N*M - 1] / 2;
    return res * h1 * h2;
}
#endif



double cNormError(const double * u, const int & M, const int & N, const double & h1, const double & h2)
{
    double norm = 0;
    for (int j = 0; j < N; j++) {
        for (int i = 0; i < M; i++)
            norm = max(norm, fabs(u[i + j*M] - u_exact(i*h1, j*h2)));
    }
    return norm;
}


void subtractTauR(const double & tau, const double * r, double * u, const int & MN)
{
    for (int i = 0; i < MN; i++)
        u[i] -= tau*r[i];
}

double sqNorm(const double * u, const int & M, const int & N, const double & h1, const double & h2)
{
    return dotProduct(u, u, M, N, h1, h2);
}

#if 0

void subtractB(const double * b, double *r, const int & MN)
{
    for (int i = 0; i < MN; i++)
        r[i] -= b[i];
}

void operatorA(const double *u, double * r, const int & M, const int & N, const double & h1, const double & h2)
{
    /*!
     * \brief computes Au = r
     */

    for (int j = 1; j < N - 1; j++)
        for (int i = 1; i < M - 1; i++)
            r[i + M*j] =  - (u[i+1 + M*j]  - 2 * u[i + M*j] + u[i-1 + M*j]) / (h1*h1)
                          - (u[i + M*(j + 1)] - 2 * u[i + M*j] + u[i + M*(j-1)]) / (h2*h2)
                          + q(i*h1, j*h2) * u[i + M*j];

    //bottom boundary
    for (int i = 1; i < M - 1; i++)
        r[i] = - 2*(u[i + M] - u[i])/(h2*h2) + (q(i*h1, 0) + 2.0/h2) * u[i]
               - (u[i - 1] - 2*u[i] + u[i + 1])/(h1*h1);

    //top boundary
    for (int i = 1; i < M - 1; i++)
        r[i + (N - 1) * M] = 2*(u[i + (N - 1) * M] - u[i + (N - 2) * M])/(h2*h2)
               + (q(i*h1, (N - 1) * h2) + 2.0/h2) * u[i + (N - 1) * M]
               - (u[i - 1 + (N - 1) * M] - 2 * u[i + (N - 1) * M] + u[i + 1 + (N - 1) * M])/(h1*h1);

    //left boundary
    for (int j = 1; j < N - 1; j++)
        r[j*M] = - 2*(u[1 + j*M] - u[j*M])/(h1*h1) + (q(0, j*h2) + 2.0/h1) * u[j*M]
                 - (u[(j - 1) * M] - 2 * u[j*M] + u[(j + 1) * M])/(h2*h2);


    //right boundary
    for (int j = 1; j < N - 1; j++)
        r[M - 1 + j*M] = 2*(u[M - 1 + j * M] - u[M - 2 + j * M])/(h1*h1)
                        + (q((M - 1)*h1, j*h2)  + 2.0/h1) * u[M - 1 + j*M]
                        - (u[M - 1 + (j-1)*M] - 2*u[M - 1 + j*M] + u[M - 1 + (j+1)*M])/(h2*h2);

    //corners
    int idx;
    //top left
    idx = (N-1)*M;
    r[idx] = - 2*(u[1 + idx] - u[idx])/(h1*h1) + 2*(u[idx] - u[idx - M])/(h2*h2)
             + (q(0, (N-1)*h2) + 2.0/h1 + 2.0/h2) * u[idx];
    //top right
    idx = M*N - 1;
    r[idx] = 2*(u[idx] - u[idx-1])/(h1*h1) + 2*(u[idx] - u[idx - M])/(h2*h2)
            + (q((M - 1)*h1, (N - 1)*h2) + 2.0/h1 + 2.0/h2) * u[idx];
    //bottom left
    idx = 0;
    r[idx] = -2*(u[idx+1] - u[idx])/(h1*h1) - 2*(u[idx + M] - u[idx])/(h2*h2)
            + (q(0,0) + 2.0/h1 + 2.0/h2)*u[idx];
    //bottom right
    idx = M - 1;
    r[idx] = 2*(u[idx] - u[idx - 1])/(h1*h1) - 2*(u[idx+M] - u[idx])/(h2*h2)
            + (q((M-1)*h1, 0) + 2.0/h1 + 2.0/h2) * u[idx];
}
#endif
void findTau(const double *u, double & tau, const int & M, const int & N, const double & h1, const double & h2)
{
    /*!
     * \brief computes tau = (Au, u)/(Au, Au)
     */

    double Ts = 0, Bs = 0;

    //corners
    int idx;

    //bottom left
    {
    idx = 0;
    const double tmp = -2*(u[idx+1] - u[idx])/(h1*h1) - 2*(u[idx + M] - u[idx])/(h2*h2)
            + (q(0,0) + 2.0/h1 + 2.0/h2)*u[idx];
    Ts += 0.5 * tmp * u[idx];
    Bs += 0.5 * tmp * tmp;
    }

    //bottom boundary
    for (int i = 1; i < M - 1; i++) {
        const double tmp = - 2*(u[i + M] - u[i])/(h2*h2) + (q(i*h1, 0) + 2.0/h2) * u[i]
               - (u[i - 1] - 2*u[i] + u[i + 1])/(h1*h1);
        Ts += 0.5 * tmp * u[i];
        Bs += 0.5 * tmp * tmp;
    }

    //bottom right
    {
    idx = M - 1;
    const double tmp = 2*(u[idx] - u[idx - 1])/(h1*h1) - 2*(u[idx+M] - u[idx])/(h2*h2)
            + (q((M-1)*h1, 0) + 2.0/h1 + 2.0/h2) * u[idx];
    Ts += 0.5 * tmp * u[idx];
    Bs += 0.5 * tmp * tmp;
    }
    //left, inner, right
    for (int j = 1; j < N - 1; j++) {

        //left
        {
        const double tmp = - 2*(u[1 + j*M] - u[j*M])/(h1*h1) + (q(0, j*h2) + 2.0/h1) * u[j*M]
                 - (u[(j - 1) * M] - 2 * u[j*M] + u[(j + 1) * M])/(h2*h2);

        Ts += 0.5 * tmp * u[j*M];
        Bs += 0.5 * tmp * tmp;
        }
        //inner
        for (int i = 1; i < M - 1; i++) {
            const double tmp =  - (u[i+1 + M*j]  - 2 * u[i + M*j] + u[i-1 + M*j]) / (h1*h1)
                          - (u[i + M*(j + 1)] - 2 * u[i + M*j] + u[i + M*(j-1)]) / (h2*h2)
                          + q(i*h1, j*h2) * u[i + M*j];

            Ts += tmp * u[i + M*j];
            Bs += tmp * tmp;
        }
        //right
        {
        const double tmp = 2*(u[M - 1 + j * M] - u[M - 2 + j * M])/(h1*h1)
                        + (q((M - 1)*h1, j*h2)  + 2.0/h1) * u[M - 1 + j*M]
                        - (u[M - 1 + (j-1)*M] - 2*u[M - 1 + j*M] + u[M - 1 + (j+1)*M])/(h2*h2);

        Ts += 0.5 * tmp * u[M - 1 + j*M];
        Bs += 0.5 * tmp * tmp;
        }

    }

    //top left
    {
    idx = (N-1)*M;
    const double tmp = - 2*(u[1 + idx] - u[idx])/(h1*h1) + 2*(u[idx] - u[idx - M])/(h2*h2)
             + (q(0, (N-1)*h2) + 2.0/h1 + 2.0/h2) * u[idx];

    Ts += 0.5 * tmp * u[idx];
    Bs += 0.5 * tmp * tmp;
    }

    //top boundary
    for (int i = 1; i < M - 1; i++) {
        const double tmp = 2*(u[i + (N - 1) * M] - u[i + (N - 2) * M])/(h2*h2)
               + (q(i*h1, (N - 1) * h2) + 2.0/h2) * u[i + (N - 1) * M]
               - (u[i - 1 + (N - 1) * M] - 2 * u[i + (N - 1) * M] + u[i + 1 + (N - 1) * M])/(h1*h1);

        Ts += 0.5 * tmp * u[i + (N - 1) * M];
        Bs += 0.5 * tmp * tmp;
    }


    //top right
    {
    idx = M*N - 1;
    const double tmp = 2*(u[idx] - u[idx-1])/(h1*h1) + 2*(u[idx] - u[idx - M])/(h2*h2)
            + (q((M - 1)*h1, (N - 1)*h2) + 2.0/h1 + 2.0/h2) * u[idx];

    Ts += 0.5 * tmp * u[idx];
    Bs += 0.5 * tmp * tmp;
    }
    //finally...
    tau = Ts/Bs;
}

void findR(const double *b, const double *u, double * r, double & FsqNorm, const int & M, const int & N, const double & h1, const double & h2)
{
    /*!
     * \brief computes r = Au - b and squared norm of r
     */
    double sqNorm = 0;
    //corners
    int idx;

    //bottom left
    idx = 0;
    sqNorm += 0.5 * pow(
    r[idx] = -2*(u[idx+1] - u[idx])/(h1*h1) - 2*(u[idx + M] - u[idx])/(h2*h2)
            + (q(0,0) + 2.0/h1 + 2.0/h2)*u[idx]
            - b[idx],
            2);

    //bottom boundary
    for (int i = 1; i < M - 1; i++)
        sqNorm += 0.5 * pow(
        r[i] = - 2*(u[i + M] - u[i])/(h2*h2) + (q(i*h1, 0) + 2.0/h2) * u[i]
               - (u[i - 1] - 2*u[i] + u[i + 1])/(h1*h1)
               - b[i],
                2);

    //bottom right
    idx = M - 1;
    sqNorm += 0.5 * pow(
    r[idx] = 2*(u[idx] - u[idx - 1])/(h1*h1) - 2*(u[idx+M] - u[idx])/(h2*h2)
            + (q((M-1)*h1, 0) + 2.0/h1 + 2.0/h2) * u[idx]
            - b[idx],
            2);

    //left, inner, right
    for (int j = 1; j < N - 1; j++) {

        //left
        sqNorm += 0.5 * pow(
        r[j*M] = - 2*(u[1 + j*M] - u[j*M])/(h1*h1) + (q(0, j*h2) + 2.0/h1) * u[j*M]
                 - (u[(j - 1) * M] - 2 * u[j*M] + u[(j + 1) * M])/(h2*h2)
                 - b[j*M],
                2);
        //inner
        for (int i = 1; i < M - 1; i++)
            sqNorm += pow(
            r[i + M*j] =  - (u[i+1 + M*j]  - 2 * u[i + M*j] + u[i-1 + M*j]) / (h1*h1)
                          - (u[i + M*(j + 1)] - 2 * u[i + M*j] + u[i + M*(j-1)]) / (h2*h2)
                          + q(i*h1, j*h2) * u[i + M*j]
                          - b[i + M*j],
                    2);
        //right
        sqNorm += 0.5 * pow(
        r[M - 1 + j*M] = 2*(u[M - 1 + j * M] - u[M - 2 + j * M])/(h1*h1)
                        + (q((M - 1)*h1, j*h2)  + 2.0/h1) * u[M - 1 + j*M]
                        - (u[M - 1 + (j-1)*M] - 2*u[M - 1 + j*M] + u[M - 1 + (j+1)*M])/(h2*h2)
                        - b[M - 1 + j*M],
                2);
    }

    //top left
    idx = (N-1)*M;
    sqNorm += 0.5 * pow(
    r[idx] = - 2*(u[1 + idx] - u[idx])/(h1*h1) + 2*(u[idx] - u[idx - M])/(h2*h2)
             + (q(0, (N-1)*h2) + 2.0/h1 + 2.0/h2) * u[idx]
             - b[idx],
            2);

    //top boundary
    for (int i = 1; i < M - 1; i++)
        sqNorm += 0.5 * pow(
        r[i + (N - 1) * M] = 2*(u[i + (N - 1) * M] - u[i + (N - 2) * M])/(h2*h2)
               + (q(i*h1, (N - 1) * h2) + 2.0/h2) * u[i + (N - 1) * M]
               - (u[i - 1 + (N - 1) * M] - 2 * u[i + (N - 1) * M] + u[i + 1 + (N - 1) * M])/(h1*h1)
               - b[i + (N - 1) * M],
                2);


    //top right
    idx = M*N - 1;
    sqNorm += 0.5 * pow(
    r[idx] = 2*(u[idx] - u[idx-1])/(h1*h1) + 2*(u[idx] - u[idx - M])/(h2*h2)
            + (q((M - 1)*h1, (N - 1)*h2) + 2.0/h1 + 2.0/h2) * u[idx]
            - b[idx],
            2);

    FsqNorm = sqNorm*h1*h2;
}


void initB(double *b, const int & M, const int & N, const double & h1, const double & h2)
{
    /*!
     * \brief computes right-hand side b
     */
    for (int j = 1; j < N - 1; j++)
        for (int i = 1; i < M - 1; i++)
            b[i + M*j] = F(i*h1, j*h2);

    //bottom boundary
    for (int i = 1; i < M - 1; i++)
        b[i] = F(i*h1, 0) + 2*psi_B(i*h1, 0)/h2;

    //top boundary
    for (int i = 1; i < M - 1; i++)
        b[i + (N - 1) * M] = F(i * h1, (N - 1) * h2) + 2*psi_T(i * h1, (N - 1) * h2)/h2;


    //left boundary
    for (int j = 1; j < N - 1; j++)
        b[j*M] = F(0, j*h2) + 2*psi_L(0, j*h2)/h1;

    //right boundary
    for (int j = 1; j < N - 1; j++)
        b[M - 1 + j*M] = F((M-1)*h1, j*h2) + 2*psi_R((M-1)*h1, j*h2)/h1;

    //corners
    int idx;
    //top left
    idx = (N-1)*M;
    b[idx] = F(0, (N-1)*h2) + 2*psi_L(0, (N-1)*h2)/h1 + 2*psi_T(0, (N-1)*h2)/h2;
    //top right
    idx = M*N - 1;
    b[idx] = F((M - 1)*h1, (N - 1)*h2) + 2*psi_R((M-1)*h1, (N-1)*h2)/h1 + 2*psi_T((M-1)*h1, (N-1)*h2)/h2;
    //bottom left
    idx = 0;
    b[idx] = F(0, 0) + 2*psi_L(0, 0)/h1 + 2*psi_B(0, 0)/h2;
    //bottom right
    idx = M - 1;
    b[idx] = F((M-1)*h1, 0) + 2*psi_R((M-1)*h1, 0)/h1 + 2*psi_B((M-1)*h1, 0)/h2;
}

int solvePoisson(double * u, int M, int N, double l1, double l2, int maxIter, int & iter, double & cerror, double & time_sec, double * dump_s=nullptr, int max_ds=0, int dump_freq=0)
{
    /*!
     * \brief Poisson equation solver on rectangle (0, 0) - (l1, l2)
     *
     * u -- solution (M x N elements should be allocated)
     * M x N -- mesh
     * l1 x l2 -- rectangular area dimensions
     */
    const int MN = M*N;
    const double h1 = l1/(M - 1);
    const double h2 = l2/(N - 1);
    double * b, * r;
    int dumped_columns = 0;
    try {
        b = new double [MN];
        r = new double [MN];
    } catch(bad_alloc &) {
        cerr << "Cant allocate memory" << endl;
        return -1;
    }

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    //init u
    for (int i = 0; i < MN; i++)
        u[i] = 0;

    //init right-hand side b
    initB(b, M, N, h1, h2);
    const double r0norm = sqrt(sqNorm(b, M, N, h1, h2));
    double rnorm;
    double tau;
  //const double eps = 1e-6;
    const double rtol = 1e-6, atol=1e-6;
    iter = 0;
    double RsqNorm;
    if (dump_s) {
        for (int j = 0; j < N; j++) {
            for (int i = 0; i < M; i++) {
                dump_s[(j*M + i)*max_ds] = i*h1;
                dump_s[(j*M + i)*max_ds + 1] = j*h2;
            }
        }
        dumped_columns = 2;
    }
    do {
        if (dump_s && dumped_columns < max_ds && iter%dump_freq == 0) {
            //dump error
            for (int j = 0; j < N; j++) {
                for (int i = 0; i < M; i++) {
                    dump_s[(j*M + i)*max_ds + dumped_columns] = fabs(u[i + M*j] - u_exact(i*h1, j*h2));
                }
            }
            dumped_columns++;
        }

        iter++;
        findR(b, u, r, RsqNorm, M, N, h1, h2);
        rnorm = sqrt(RsqNorm);
        findTau(r, tau, M, N, h1, h2);
        subtractTauR(tau, r, u, MN);
        if (iter > maxIter) break;
    } while (rnorm > atol || rnorm > rtol * r0norm);
    //} while (rnorm > eps/fabs(tau)); //doesnt work well
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::chrono::duration<double> time = end - begin;
    time_sec = time.count();
    cerror =  cNormError(u, M, N, h1, h2);
    delete [] b;
    delete [] r;
    return dumped_columns;
}

void saveError(const string & filename, const double * u, int M, int N, double h1, double h2)
{
    //export error
    ofstream file(filename);
    if (!file.is_open()) {
        cerr << "Cant open file to save error" << endl;
        return;
    }
    for (int i = 0 ; i < M; i++) {
        for (int j = 0; j < N; j++)
            file << i*h1 << " " << j*h2 << " " << fabs(u[i + M*j] - u_exact(i*h1, j*h2)) << endl;
        file << endl;
    }
    file.close();
}

void saveFullDump(const string & filename, const double * u, int M, int N, int Nmax, int newLine)
{
    //use to save error dump
    ofstream file(filename);
    if (!file.is_open()) {
        cerr << "Cant open file to save error" << endl;
        return;
    }
    for (int i = 0 ; i < M; i++) {
        if (i % newLine == 0)
            file << endl;
        for (int j = 0; j < N; j++)
            file << u[i*Nmax + j] << " ";
        file << endl;
    }
    file.close();
}

int main(void)
{
    const int maxIter = 1000000;
    const double l1 = 4, l2 = 3;//rectangle dimensions
    int mesh[] = {40};
    for (auto & e: mesh) {
        const int M = e, N = e; //number of nodes along x and y
        const double h1 = l1/(M-1), h2 = l2/(N-1);
        double * u; //numerical solution
        try {
            u = new double [M*N];
        } catch(bad_alloc &) {
            cerr << "Cant allocate memory" << endl;
            return -1;
        }
        int iter = 0;
        double cerror = 999;
        double m_time = 0;
        int tries = 3;
        for (int i = 0; i < tries; i++) {
            double time;
            if (i == 0) {
                int max_col = 1000;
                double * dump_e = new double [M*N*max_col];
                int dump_freq = 10;
                int cols = solvePoisson(u, M, N, l1, l2, maxIter, iter, cerror, time, dump_e, max_col, dump_freq);
                cout << "COLS: " << cols << endl;
                saveFullDump("DUMP.dat", dump_e, M*N, cols, max_col, M);
                delete [] dump_e;
            } else {
                solvePoisson(u, M, N, l1, l2, maxIter, iter, cerror, time);
            }
            m_time += time;
        }
        m_time /= tries;
        cout << "Mesh " << M << "x" << N << " elements" << endl;
        if (iter > maxIter) {
            cout << "Number of iterations reached the limit" << endl;
        } else {
            cout << "C-error: " << cerror << endl;
            cout << "Iters: " << iter << endl;
            cout << "Time (s): " << m_time << endl;
        }
        cout << endl;
        saveError(string("Error") + to_string(e) + ".txt", u, M, N, h1, h2);
        delete [] u;
    }
    return 0;
}

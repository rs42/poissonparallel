#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>
#include <mpi.h>
#ifdef SOPENMP
#include <omp.h>
#endif

using namespace std;

const double l1 = 4, l2 = 3; //rectangle dimensions
inline double u_exact(const double & x, const double & y)
{
    /*!
     * \brief Exact solution of Poisson equation - \Delta u + q u = F
     *        on a 2D rectange (0, 0) - (l1, l2) with third type boundary conditions
     */
    return sqrt(4 + x * y);
}
inline double psi_L(const double & x, const double & y)
{
   // assert(fabs(x) < 1e-8);
    const double t = sqrt(4 + x * y);
    return - y/t/2 + t;
}
inline double psi_R(const double & x, const double & y)
{
   // assert(fabs(x - 4) < 1e-8);
    const double t = sqrt(4 + x * y);
    return  y/t/2 + t;
}
inline double psi_B(const double & x, const double & y)
{
   // assert(fabs(y) < 1e-8);
    const double t = sqrt(4 + x * y);
    return - x/t/2 + t;
}
inline double psi_T(const double & x, const double & y)
{
   // assert(fabs(y - 3) < 1e-8 );
    const double t = sqrt(4 + x * y);
    return  x/t/2 + t;
}
inline double F(const double &x, const double &y)
{
    return sqrt(4 + x * y) * ( (x*x + y*y) * pow(4 + x * y, -2)/4  + x + y);
}
inline double q(const double &x, const double &y)
{
    return x + y;
}

const int master_world_rank = 0;

struct Block
{
    /*!
     * \brief Block is a part of a rectangle
     */
    //one process owns one block
    int world_rank;
    int world_size;
    
    MPI_Comm cartesian_comm;
    int rank_2d;
    int coords_2d[2];
    int dims[2];
    //neighbor's ranks, no neighbor value is MPI_PROC_NULL
    int left_neighbor, right_neighbor, up_neighbor, down_neighbor;
    

    //data about area
    int i_0, j_0; //offset from global (0,0)
    int M, N;
    int B_p, T_p, R_p, L_p; //paddings for additional data, u and r need them
    int neighbors_count;
    MPI_Request send_requests[4];
    MPI_Request recv_requests[4];

    int main_i_sz, main_j_sz; //dims of main data
    int total_i_sz, total_j_sz; //whole dims

    double h1, h2;
    double * u, * b, * r;
    double *send_left_b, *recv_left_b,//need to allocate memory
           *send_right_b, *recv_right_b;
    double *Psend_top_b, *Precv_top_b, //no (!) need to allocate memory 
           *Psend_bot_b, *Precv_bot_b;

    inline double loc_f(double (*f)(const double &, const double &), const int & i, const int & j) const
    {
        return f((i + i_0) * h1, (j + j_0) * h2);
    }
    inline double & b_at(const int & i, const int & j)
    {
        return b[i + j * main_i_sz];
    }
    inline double & u_at(const int & i, const int & j)
    {
        return u[i + L_p + (j + B_p) * total_i_sz];
    }
    inline double & r_at(const int & i, const int & j)
    {
        /*
        if ( i + L_p + (j + B_p) * total_i_sz < 0)
        {
            cout << "LESS THAN 0: " << coords_2d[0] << " " << coords_2d[1] << " " << i << " " << j << endl;
            assert(0);
        }
        if (i + L_p + (j + B_p) * total_i_sz >= total_i_sz*total_j_sz)
        {
            cout << "TOOO BIG: " << coords_2d[0] << " " << coords_2d[1] << " " << i << " " << j << endl;
            assert(0);
        }
        */
        return r[i + L_p + (j + B_p) * total_i_sz];
    }
    
    void solvePoisson(int & iter, double & time_all, double & time_msg, double & cerror);

    template<typename Op>
    void iterateOverBlock(Op & op);

    void initMPI()
    {
        MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
        MPI_Comm_size(MPI_COMM_WORLD, &world_size);
        if (world_rank == master_world_rank) {
            cout << "MPI process " << world_rank << "/" << world_size << endl;

#ifdef SOPENMP
        cout << "OpenMP procs: " << omp_get_max_threads() << endl;
#else
        cout << "No OpenMP support" << endl;
#endif
        }
        dims[0] = dims[1] = 0; //controlled by MPI_Dims_create
        MPI_Dims_create(world_size, 2, dims);
        int periods[2] = {0, 0}; //no periods!
        if (world_rank == master_world_rank)
            cout << "DIMS: " << dims[0] << " " << dims[1] << endl;
        MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &cartesian_comm);

        MPI_Comm_rank(cartesian_comm, &rank_2d);
        MPI_Cart_coords(cartesian_comm, rank_2d, 2, coords_2d);

        MPI_Cart_shift(cartesian_comm, 0, 1, &left_neighbor, &right_neighbor);
        MPI_Cart_shift(cartesian_comm, 1, 1, &down_neighbor, &up_neighbor);

        //cout << "Process " << rank_2d << " x: " << coords_2d[0] << " y: " << coords_2d[1] << " N: " <<
        //    left_neighbor << " " << right_neighbor << " " << down_neighbor << " " << up_neighbor << endl;
        u = b = r = nullptr;
        send_left_b = recv_left_b = send_right_b = recv_right_b =
            Psend_top_b = Precv_top_b = Psend_bot_b = Precv_bot_b
            = nullptr;
    }
    void free_bufs()
    {
        if (u)
            delete [] u;
        if (b)
            delete [] b;
        if (r)
            delete [] r;
        if (send_left_b)
            delete [] send_left_b;
        if (recv_left_b)
            delete [] recv_left_b;
        if (send_right_b)
            delete [] send_right_b;
        if (recv_right_b)
            delete [] recv_right_b;
    }
    void initPoisson(int MM, int NN)
    {
        M = MM;
        N = NN;
        //free prev bufs
        free_bufs();
        u = b = r = nullptr;
        send_left_b = recv_left_b = send_right_b = recv_right_b = 
            Psend_top_b = Precv_top_b = Psend_bot_b = Precv_bot_b
            = nullptr;
        
        h1 = l1/(M-1);
        h2 = l2/(N-1);
    
        const int mx_1proc = (M + dims[0] - 1)/dims[0];
        const int ny_1proc = (N + dims[1] - 1)/dims[1];
    
        if (dims[0] > M || dims[1] > N) {
            if (world_rank == master_world_rank)
                cerr << "Too many mpi processes" << endl;
            throw(0);
        }

        i_0 = mx_1proc * coords_2d[0];
        j_0 = ny_1proc * coords_2d[1];
        const int i_max = min(M, i_0 + mx_1proc);
        const int j_max = min(N, j_0 + ny_1proc);
        main_i_sz = i_max - i_0;
        main_j_sz = j_max - j_0;
        


        if (right_neighbor == MPI_PROC_NULL) {
            assert(coords_2d[0] == dims[0] - 1);
            R_p = 0;
        } else {
            assert(coords_2d[0] < dims[0] - 1);
            R_p = 1;
            send_right_b = new double [main_j_sz];
            recv_right_b = new double [main_j_sz];
        }
           
        if (left_neighbor == MPI_PROC_NULL) {
            assert(coords_2d[0] == 0);
            L_p = 0;
        } else {
            assert(coords_2d[0] > 0);
            L_p = 1;
            send_left_b = new double [main_j_sz];
            recv_left_b = new double [main_j_sz];
        }

        if (up_neighbor == MPI_PROC_NULL) {
            assert(coords_2d[1] == dims[1] - 1);
            T_p = 0;
        } else {
            assert(coords_2d[1] < dims[1] - 1);
            T_p = 1;
        }
        
        if (down_neighbor == MPI_PROC_NULL) {
            assert(coords_2d[1] == 0);
            B_p = 0;
        } else {
            assert(coords_2d[1] > 0);
            B_p = 1;
        }
        neighbors_count = B_p + T_p + L_p + R_p; 
        total_i_sz = main_i_sz + R_p + L_p;
        total_j_sz = main_j_sz + T_p + B_p;

        u = new double [total_i_sz * total_j_sz];
        r = new double [total_i_sz * total_j_sz];
        b = new double [main_i_sz * main_j_sz];
        
        if (T_p) {
            Psend_top_b = r + total_i_sz * (total_j_sz - 2) + L_p;
            Precv_top_b = r + total_i_sz * (total_j_sz - 1) + L_p;
        }
        if (B_p) {
            Precv_bot_b = r + L_p;
            Psend_bot_b = r + total_i_sz + L_p;
        }
    }
    void waitSend()
    {
        if (neighbors_count)
            MPI_Waitall(neighbors_count, send_requests, MPI_STATUSES_IGNORE);
    }
    void waitRecv()
    {
        if (neighbors_count) {
            MPI_Waitall(neighbors_count, recv_requests, MPI_STATUSES_IGNORE);
            //save received parts of r
            //left
            if (L_p) {
                for (int j = 0; j < main_j_sz; j++)
                    r_at(-1, j) = recv_left_b[j];
            }
            //right
            if (R_p) {
                for (int j = 0; j < main_j_sz; j++) {
                    r_at(main_i_sz, j) = recv_right_b[j];
                }
            }
        }
    }
    void sendRecR()
    {
        //prepare left and right buffers first
        if (L_p) {
            for (int j = 0; j < main_j_sz; j++) {
                send_left_b[j] = r_at(0, j);
            }
        }
        if (R_p) {
            for (int j = 0; j < main_j_sz; j++)
                send_right_b[j] = r_at(main_i_sz - 1, j);
        }

        int n = 0;
        if (T_p) {
            MPI_Isend(Psend_top_b, main_i_sz, MPI_DOUBLE, up_neighbor, 0,
                     cartesian_comm, &send_requests[n]);
            MPI_Irecv(Precv_top_b, main_i_sz, MPI_DOUBLE, up_neighbor, 0,
                     cartesian_comm, &recv_requests[n++]);
        }
        if (B_p) {
            MPI_Isend(Psend_bot_b, main_i_sz, MPI_DOUBLE, down_neighbor, 0,
                     cartesian_comm, &send_requests[n]);
            MPI_Irecv(Precv_bot_b, main_i_sz, MPI_DOUBLE, down_neighbor, 0,
                     cartesian_comm, &recv_requests[n++]);
        }
        if (L_p) {
            //fill buffer first 
            MPI_Isend(send_left_b, main_j_sz, MPI_DOUBLE, left_neighbor, 0,
                     cartesian_comm, &send_requests[n]);
            MPI_Irecv(recv_left_b, main_j_sz, MPI_DOUBLE, left_neighbor, 0,
                     cartesian_comm, &recv_requests[n++]);
        }
        if (R_p) {
            //fill buffer first
            MPI_Isend(send_right_b, main_j_sz, MPI_DOUBLE, right_neighbor, 0,
                     cartesian_comm, &send_requests[n]);
            MPI_Irecv(recv_right_b, main_j_sz, MPI_DOUBLE, right_neighbor, 0,
                     cartesian_comm, &recv_requests[n++]);
        }
    }

    ~Block()
    {
        MPI_Comm_free(&cartesian_comm);
        free_bufs();
    }
   
    double cNormError()
    {
        double norm = 0, fnorm;
#ifdef SOPENMP
    #pragma omp parallel for reduction(max:norm)
#endif
        for (int j = 0; j < main_j_sz; j++) {
            for (int i = 0; i < main_i_sz; i++)
                norm = max(norm, fabs(u_at(i, j) - loc_f(u_exact, i, j)));
        }
        MPI_Allreduce(&norm, &fnorm, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        return fnorm;
    }

    void subtractTauR(const double & tau)
    {//openmp usage here is debatable
#ifdef SOPENMPG
    #pragma omp parallel for
#endif
        for (int i = 0; i < total_i_sz * total_j_sz; i++)
            u[i] -= tau*r[i];
    }
    void initU()
    {//openmp usage here is debatable
#ifdef SOPENMPG
    #pragma omp parallel for
#endif
        for (int i = 0; i < total_i_sz * total_j_sz; i++)
            u[i] = 0;
    }
    int storeU(double * fu)
    { 
        if (!fu) {
            //send block to master
            int buf_sz = main_i_sz*main_j_sz + 4;
            double *buf = new double [buf_sz];
            for (int j = 0; j < main_j_sz; j++)
                for (int i = 0; i < main_i_sz; i++)
                    buf[i+j*main_i_sz] = u_at(i, j);
            buf[buf_sz - 4] = i_0;
            buf[buf_sz - 3] = j_0;
            buf[buf_sz - 2] = main_i_sz;
            buf[buf_sz - 1] = main_j_sz;
            MPI_Send(buf, buf_sz, MPI_DOUBLE, master_world_rank, 0, MPI_COMM_WORLD);
            delete [] buf;
        } else {
            const int mx_1proc = (M + dims[0] - 1)/dims[0];
            const int ny_1proc = (N + dims[1] - 1)/dims[1];
            //master receives
            double *buf = new double [mx_1proc * ny_1proc + 4];
            for (int proc = 1; proc < world_size; proc++) {
                MPI_Status status;
                MPI_Recv(buf, mx_1proc * ny_1proc + 4, MPI_DOUBLE, proc, 0, MPI_COMM_WORLD, &status);
                int msg_sz;
                MPI_Get_count(&status, MPI_DOUBLE, &msg_sz);
                int j_sz = buf[msg_sz - 1];
                int i_sz = buf[msg_sz - 2];
                int j_start = buf[msg_sz - 3];
                int i_start = buf[msg_sz - 4];
                for (int j = 0; j < j_sz; j++)
                    for (int i = 0; i < i_sz; i++)
                        fu[i_start + i + M*(j_start + j)] = buf[i+j*i_sz];
            }
            //also save master's data as well
            for (int j = 0; j < main_j_sz; j++)
                for (int i = 0; i < main_i_sz; i++)
                    fu[i_0 + i + M*(j_0 + j)] = u_at(i, j);

            delete [] buf;
        }
        return 1;
    }
} block;



struct FindTau
{
    /*!
     * \brief computes tau = (Ar, r)/(Ar, Ar)
     */

    double Ts, Bs;
    double tau;
    const double h1 = block.h1;
    const double h2 = block.h2;
    const int main_i_sz = block.main_i_sz;
    const int main_j_sz = block.main_j_sz;
    FindTau()
    {
        Ts = Bs = 0;
    }
    void reset()
    {
        Ts = Bs = 0;
    }
    FindTau& operator+=(const FindTau& other)
    {
        Ts += other.Ts;
        Bs += other.Bs;
        return *this;
    }
    void finalize()
    {
        double bt[2];
        bt[0] = Ts;
        bt[1] = Bs;
        double res[2];
        MPI_Allreduce(bt, res, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        tau = res[0]/res[1];
    }
    void bottom_left()
    {
        const double tmp = -2*(block.r_at(1, 0) - block.r_at(0, 0))/(h1*h1) - 2*(block.r_at(0, 1) - block.r_at(0, 0))/(h2*h2)
                         + (block.loc_f(q, 0, 0) + 2.0/h1 + 2.0/h2)*block.r_at(0, 0);
        Ts += 0.5 * tmp * block.r_at(0, 0);
        Bs += 0.5 * tmp * tmp;
    }
    void bottom(const int & i)
    {
        const double tmp = - 2*(block.r_at(i, 1) - block.r_at(i, 0))/(h2*h2) + (block.loc_f(q, i, 0) + 2.0/h2) * block.r_at(i, 0)
                         - (block.r_at(i - 1, 0) - 2*block.r_at(i, 0) + block.r_at(i + 1, 0))/(h1*h1);
        Ts += 0.5 * tmp * block.r_at(i, 0);
        Bs += 0.5 * tmp * tmp;

    }
    void bottom_right()
    {
        const double tmp = 2*(block.r_at(main_i_sz - 1, 0) - block.r_at(main_i_sz - 2, 0))/(h1*h1)
                         - 2*(block.r_at(main_i_sz - 1, 1) - block.r_at(main_i_sz - 1, 0))/(h2*h2)
                         + (block.loc_f(q, main_i_sz - 1, 0) + 2.0/h1 + 2.0/h2) * block.r_at(main_i_sz - 1, 0);
        Ts += 0.5 * tmp * block.r_at(main_i_sz - 1, 0);
        Bs += 0.5 * tmp * tmp;
    }
    void left(const int & j)
    {
        const double tmp = - 2*(block.r_at(1, j) - block.r_at(0, j))/(h1*h1) + (block.loc_f(q, 0, j) + 2.0/h1) * block.r_at(0, j)
                         - (block.r_at(0, j - 1) - 2 * block.r_at(0, j) + block.r_at(0, j + 1))/(h2*h2);

        Ts += 0.5 * tmp * block.r_at(0, j);
        Bs += 0.5 * tmp * tmp;
    }
    void inner(const int & i, const int & j)
    {
        const double tmp =  - (block.r_at(i+1, j)  - 2 * block.r_at(i, j) + block.r_at(i-1, j)) / (h1*h1)
                         - (block.r_at(i, j + 1) - 2 * block.r_at(i, j) + block.r_at(i, j-1)) / (h2*h2)
                         + block.loc_f(q, i, j) * block.r_at(i, j);

        Ts += tmp * block.r_at(i, j);
        Bs += tmp * tmp;
    }
    void right(const int & j)
    {
        const double tmp = 2*(block.r_at(main_i_sz - 1, j) - block.r_at(main_i_sz - 2, j))/(h1*h1)
                         + (block.loc_f(q, main_i_sz - 1, j)  + 2.0/h1) * block.r_at(main_i_sz - 1, j)
                         - (block.r_at(main_i_sz - 1, j-1) - 2*block.r_at(main_i_sz - 1, j)
                         + block.r_at(main_i_sz - 1, j+1))/(h2*h2);

        Ts += 0.5 * tmp * block.r_at(main_i_sz - 1, j);
        Bs += 0.5 * tmp * tmp;
    }
    void top_left()
    {
        const double tmp = - 2*(block.r_at(1, main_j_sz - 1) - block.r_at(0, main_j_sz - 1))/(h1*h1)
                         + 2*(block.r_at(0, main_j_sz - 1) - block.r_at(0, main_j_sz - 2))/(h2*h2)
                         + (block.loc_f(q, 0, main_j_sz - 1) + 2.0/h1 + 2.0/h2) * block.r_at(0, main_j_sz - 1);

        Ts += 0.5 * tmp * block.r_at(0, main_j_sz - 1);
        Bs += 0.5 * tmp * tmp;
    }
    void top(const int & i)
    {
        const double tmp = 2*(block.r_at(i, main_j_sz - 1) - block.r_at(i, main_j_sz - 2))/(h2*h2)
                         + (block.loc_f(q, i, main_j_sz - 1) + 2.0/h2) * block.r_at(i, main_j_sz - 1)
                         - (block.r_at(i - 1, main_j_sz - 1) - 2 * block.r_at(i, main_j_sz - 1) 
                         + block.r_at(i + 1, main_j_sz - 1))/(h1*h1);

        Ts += 0.5 * tmp * block.r_at(i, main_j_sz - 1);
        Bs += 0.5 * tmp * tmp;
    }
    void top_right()
    {
        const double tmp = 2*(block.r_at(main_i_sz - 1, main_j_sz - 1) - block.r_at(main_i_sz - 2, main_j_sz - 1))/(h1*h1)
                         + 2*(block.r_at(main_i_sz - 1, main_j_sz - 1) - block.r_at(main_i_sz - 1, main_j_sz - 2))/(h2*h2)
                         + (block.loc_f(q, main_i_sz - 1, main_j_sz - 1)
                         + 2.0/h1 + 2.0/h2) * block.r_at(main_i_sz - 1, main_j_sz - 1);

        Ts += 0.5 * tmp * block.r_at(main_i_sz - 1, main_j_sz - 1);
        Bs += 0.5 * tmp * tmp;
    }
};
#ifdef SOPENMP
#pragma omp declare reduction(+: FindTau: \
	omp_out += omp_in)
#endif

struct InitB
{
    /*!
     * \brief computes right-hand side b and its squared norm
     */

    const double h1 = block.h1;
    const double h2 = block.h2;
    const int main_i_sz = block.main_i_sz;
    const int main_j_sz = block.main_j_sz;
    double sqNorm, FsqNorm;
    InitB()
    {
        sqNorm = 0;
    }
    void reset()
    {
        sqNorm = 0;
    }
	InitB& operator += (const InitB & other)
	{
		sqNorm += other.sqNorm;
		return *this;
	}
    void finalize()
    {
        MPI_Allreduce(&sqNorm, &FsqNorm, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        FsqNorm *= h1*h2;
    }
    void bottom_left()
    {
        sqNorm += 0.5 * pow(
            block.b_at(0, 0) = block.loc_f(F, 0, 0) + 2 * block.loc_f(psi_L, 0, 0) / h1
                                        + 2 * block.loc_f(psi_B, 0, 0) / h2,
            2);
        
    }
    void bottom(const int & i)
    {
        sqNorm += 0.5 * pow(
            block.b_at(i, 0) = block.loc_f(F, i, 0) + 2 * block.loc_f(psi_B, i, 0) / h2,
            2);
    }
    void bottom_right()
    {
        sqNorm += 0.5 * pow(
                block.b_at(main_i_sz - 1, 0) = block.loc_f(F, main_i_sz - 1, 0) 
                +  2 * block.loc_f(psi_R, main_i_sz - 1, 0) / h1 
                +  2 * block.loc_f(psi_B, main_i_sz - 1, 0) / h2,
                2);
    }
    void left(const int & j)
    {
        sqNorm += 0.5 * pow(
                block.b_at(0, j) = block.loc_f(F, 0, j) + 2 * block.loc_f(psi_L, 0, j) / h1,
            2);
    }
    void inner(const int & i, const int & j)
    {
        sqNorm += pow(block.b_at(i,j) = block.loc_f(F, i, j), 2);
    }
    void right(const int & j)
    {
        sqNorm += 0.5 * pow(
                block.b_at(main_i_sz - 1, j) = block.loc_f(F, main_i_sz - 1, j)
                                        + 2 * block.loc_f(psi_R, main_i_sz - 1, j)/h1,
                2);
    }
    void top_left()
    {
        sqNorm += 0.5 * pow(
                block.b_at(0, main_j_sz - 1) = block.loc_f(F, 0, main_j_sz - 1)
                                        + 2 * block.loc_f(psi_L, 0, main_j_sz - 1) / h1
                                        + 2 * block.loc_f(psi_T, 0, main_j_sz - 1) / h2,
                2);
    }
    void top(const int & i)
    {
        sqNorm += 0.5 * pow(
                block.b_at(i, main_j_sz - 1) = block.loc_f(F, i, main_j_sz - 1)
                                        + 2 * block.loc_f(psi_T, i, main_j_sz - 1) / h2,
                2);
    }
    void top_right()
    {
        sqNorm += 0.5 * pow(
            block.b_at(main_i_sz - 1, main_j_sz - 1) =
                block.loc_f(F, main_i_sz - 1, main_j_sz - 1)
                + 2 * block.loc_f(psi_R, main_i_sz - 1, main_j_sz - 1) / h1
                + 2 * block.loc_f(psi_T, main_i_sz - 1, main_j_sz - 1) / h2,
            2);
    }

};
#ifdef SOPENMP
#pragma omp declare reduction(+: InitB: \
	omp_out += omp_in)
#endif
struct FindR
{
    /*!
     * \brief computes r = Au - b and squared norm of r
     */
    
    const double h1 = block.h1;
    const double h2 = block.h2;
    const int  main_i_sz = block.main_i_sz;
    const int main_j_sz = block.main_j_sz;
    double sqNorm, FsqNorm;
    FindR()
    {
        sqNorm = 0;
    }
    void reset()
    {
        sqNorm = 0;
    }
    FindR& operator+=(const FindR& other)
    {
        sqNorm += other.sqNorm;
        return *this;
    }
    void finalize()
    {
        MPI_Allreduce(&sqNorm, &FsqNorm, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        FsqNorm *= h1*h2;
    }
    void bottom_left()
    {
        sqNorm += 0.5 * pow(
            block.r_at(0, 0) = -2*(block.u_at(1, 0) - block.u_at(0, 0))/(h1*h1) - 2*(block.u_at(0, 1) - block.u_at(0, 0))/(h2*h2)
                         + (block.loc_f(q, 0, 0) + 2.0/h1 + 2.0/h2)*block.u_at(0, 0)
                         - block.b_at(0, 0),
                         2);
    }
    void bottom(const int & i)
    {
        sqNorm += 0.5 * pow(
            block.r_at(i, 0) = - 2*(block.u_at(i, 1) - block.u_at(i, 0))/(h2*h2) + (block.loc_f(q, i, 0) + 2.0/h2) * block.u_at(i, 0)
                         - (block.u_at(i - 1, 0) - 2*block.u_at(i, 0) + block.u_at(i + 1, 0))/(h1*h1)
                         - block.b_at(i, 0),
                         2);
    }
    void bottom_right()
    {
        sqNorm += 0.5 * pow(
            block.r_at(main_i_sz - 1, 0) = 2*(block.u_at(main_i_sz - 1, 0) - block.u_at(main_i_sz - 2, 0))/(h1*h1)
                         - 2*(block.u_at(main_i_sz - 1, 1) - block.u_at(main_i_sz - 1, 0))/(h2*h2)
                         + (block.loc_f(q, main_i_sz - 1, 0) + 2.0/h1 + 2.0/h2) * block.u_at(main_i_sz - 1, 0)
                         - block.b_at(main_i_sz - 1, 0),
                         2);
    }
    void left(const int & j)
    {
        sqNorm += 0.5 * pow(
            block.r_at(0, j) = - 2*(block.u_at(1, j) - block.u_at(0, j))/(h1*h1) + (block.loc_f(q, 0, j) + 2.0/h1) * block.u_at(0, j)
                         - (block.u_at(0, j - 1) - 2 * block.u_at(0, j) + block.u_at(0, j + 1))/(h2*h2)
                         - block.b_at(0, j),
                         2);
    }
    void inner(const int & i, const int & j)
    {
        sqNorm += pow(
            block.r_at(i, j) = - (block.u_at(i+1, j)  - 2 * block.u_at(i, j) + block.u_at(i-1, j)) / (h1*h1)
                         - (block.u_at(i, j + 1) - 2 * block.u_at(i, j) + block.u_at(i, j-1)) / (h2*h2)
                         + block.loc_f(q, i, j) * block.u_at(i, j)
                         - block.b_at(i, j),
                         2);
    }
    void right(const int & j)
    {
        sqNorm += 0.5 * pow(
            block.r_at(main_i_sz - 1, j) = 2*(block.u_at(main_i_sz - 1, j) - block.u_at(main_i_sz - 2, j))/(h1*h1)
                         + (block.loc_f(q, main_i_sz - 1, j)  + 2.0/h1) * block.u_at(main_i_sz - 1, j)
                         - (block.u_at(main_i_sz - 1, j-1) - 2*block.u_at(main_i_sz - 1, j)
                         + block.u_at(main_i_sz - 1, j+1))/(h2*h2)
                         - block.b_at(main_i_sz - 1, j),
                         2);
    }
    void top_left()
    {
        sqNorm += 0.5 * pow(
            block.r_at(0, main_j_sz - 1) = - 2*(block.u_at(1, main_j_sz - 1) - block.u_at(0, main_j_sz - 1))/(h1*h1)
                         + 2*(block.u_at(0, main_j_sz - 1) - block.u_at(0, main_j_sz - 2))/(h2*h2)
                         + (block.loc_f(q, 0, main_j_sz - 1) + 2.0/h1 + 2.0/h2) * block.u_at(0, main_j_sz - 1)
                         - block.b_at(0, main_j_sz - 1),
                         2);
    }
    void top(const int & i)
    {
        sqNorm += 0.5 * pow(
            block.r_at(i, main_j_sz - 1) = 2*(block.u_at(i, main_j_sz - 1) - block.u_at(i, main_j_sz - 2))/(h2*h2)
                         + (block.loc_f(q, i, main_j_sz - 1) + 2.0/h2) * block.u_at(i, main_j_sz - 1)
                         - (block.u_at(i - 1, main_j_sz - 1) - 2 * block.u_at(i, main_j_sz - 1) 
                         + block.u_at(i + 1, main_j_sz - 1))/(h1*h1)
                         - block.b_at(i, main_j_sz - 1),
                         2);
    }
    void top_right()
    {
        sqNorm += 0.5 * pow(
            block.r_at(main_i_sz - 1, main_j_sz - 1) = 2*(block.u_at(main_i_sz - 1, main_j_sz - 1)
                         - block.u_at(main_i_sz - 2, main_j_sz - 1))/(h1*h1)
                         + 2*(block.u_at(main_i_sz - 1, main_j_sz - 1) - block.u_at(main_i_sz - 1, main_j_sz - 2))/(h2*h2)
                         + (block.loc_f(q, main_i_sz - 1, main_j_sz - 1)
                         + 2.0/h1 + 2.0/h2) * block.u_at(main_i_sz - 1, main_j_sz - 1)
                         - block.b_at(main_i_sz - 1, main_j_sz - 1),
                         2);
        
    }
};
#ifdef SOPENMP
#pragma omp declare reduction(+: FindR: \
	omp_out += omp_in)
#endif
template<typename Op>
void Block::iterateOverBlock(Op & op)
{
    op.reset();
    //bottom left corner
    if (!B_p && !L_p) {
        op.bottom_left();
    }
    //bottom boundary
    if (!B_p) {
#ifdef SOPENMPG
        Op tmp;
       // tmp.init();
#pragma omp parallel for reduction(+:tmp)
        for (int i = !L_p; i < main_i_sz - !R_p; i++) {
            tmp.bottom(i);
        }
        op += tmp;
#else
        for (int i = !L_p; i < main_i_sz - !R_p; i++) {
            op.bottom(i);
        }
#endif
    }
    //bottom right corner
    if (!B_p && !R_p) {
        op.bottom_right();
    }
    //left, inner, right
#ifdef SOPENMP
{
    Op tmp;
    //tmp.init();
#pragma omp parallel for reduction(+:tmp)
    for (int j = !B_p; j < main_j_sz - !T_p; j++) {
        //left
        if (!L_p) {
            tmp.left(j);
        }
        //inner
        for (int i = !L_p; i < main_i_sz - !R_p; i++) {
            tmp.inner(i, j);
        }
        //right
        if (!R_p) {
            tmp.right(j);
        }
    }
    op += tmp;
}
#else
    for (int j = !B_p; j < main_j_sz - !T_p; j++) {
        //left
        if (!L_p) {
            op.left(j);
        }
        //inner
        for (int i = !L_p; i < main_i_sz - !R_p; i++) {
            op.inner(i, j);
        }
        //right
        if (!R_p) {
            op.right(j);
        }
    }
#endif
    //top left
    if (!L_p && !T_p) {
        op.top_left();
    }
    //top boundary
    if (!T_p) {
#ifdef SOPENMPG
        Op tmp;
        //tmp.init();
#pragma omp parallel for reduction(+:tmp)
        for (int i = !L_p; i < main_i_sz - !R_p; i++) {
            tmp.top(i);
        }
        op += tmp;
#else
        for (int i = !L_p; i < main_i_sz - !R_p; i++) {
            op.top(i);
        }
#endif
    }
    //top right
    if (!T_p && !R_p) {
        op.top_right();
    }
    op.finalize();
}


void Block::solvePoisson(int & iter, double & time_all, double & time_msg, double & cerror)
{
    /*!
     * \brief Poisson equation solver on rectangle (0, 0) - (l1, l2)
     *
     * ur -- solution (M x N elements should be allocated) stored at master
     *      process
     * M x N -- mesh
     * l1 x l2 -- rectangular area dimensions
     * iter is -1 if the number of iterations reached the limit
     */
    time_all = MPI_Wtime();
    time_msg = 0;
    initU();
    InitB initB;
    iterateOverBlock(initB);
    const double r0norm = sqrt(initB.FsqNorm);
    double rnorm;
    double tau;
  //const double eps = 1e-6;
    const double rtol = 1e-6, atol=1e-6;
    iter = 0;
    const int maxIter = 10000000;

    FindTau findTau;
    FindR findR;
    do {
        iter++;

        iterateOverBlock(findR);
        rnorm = sqrt(findR.FsqNorm);

        double t_msg_t = MPI_Wtime();
        sendRecR();
        waitRecv();
        t_msg_t = MPI_Wtime() - t_msg_t;
        time_msg += t_msg_t;

        iterateOverBlock(findTau);

        tau = findTau.tau;
        subtractTauR(tau);

        t_msg_t = MPI_Wtime();
        waitSend();
        t_msg_t = MPI_Wtime() - t_msg_t;
        time_msg += t_msg_t;

        if (iter > maxIter) break;
    } while (rnorm > atol || rnorm > rtol * r0norm);
    //} while (rnorm > eps/fabs(tau)); //doesnt work well
    time_all = MPI_Wtime() - time_all;
    if (iter > maxIter) iter = -1;
    cerror = cNormError();
}

void saveError(const string & filename, const double * u, int M, int N, double h1, double h2)
{
    //export error
    ofstream file(filename);
    if (!file.is_open()) {
        cerr << "Cant open file to save error" << endl;
        return;
    }
    for (int i = 0 ; i < M; i++) {
        for (int j = 0; j < N; j++)
            file << i*h1 << " " << j*h2 << " " << fabs(u[i + M*j] - u_exact(i*h1, j*h2)) << endl;
        file << endl;
    }
    file.close();
}

int main(int argc, char **argv)
{
    assert(0);
    MPI_Init(&argc, &argv);
    int mesh[] = {20, 40, 80, 160, 500, 1000};
    block.initMPI();
    for (auto & e: mesh) {
        const int M = e, N = e;
        const double h1 = l1/(M-1), h2 = l2/(N-1);
        try {
            block.initPoisson(M, N);
        } catch(...) {
            MPI_Finalize();
            return -1;
        }
        int iter = 0;
        double time_all = 0, time_msg = 0, cerror;
        int tries;
        if (e < 2)
            tries = 3;
        else
            tries = 1;
        for (int i = 0; i < tries; i++) {
            int t_iter = 0;
            double t_time_all = 0, t_time_msg = 0, t_cerror = 999;
            MPI_Barrier(MPI_COMM_WORLD);
            block.solvePoisson(t_iter, t_time_all, t_time_msg, t_cerror);
            time_all += t_time_all;
            time_msg += t_time_msg;
            if (i == 0) {
                iter = t_iter;
                cerror = t_cerror;
            } else {
                assert(iter == t_iter);
                assert(t_cerror = cerror);
            }
        }
        time_all /= tries;
        time_msg /= tries;

        if (block.world_rank == master_world_rank) {
            cout << "Mesh " << M << "x" << N << " elements" << endl;
            if (iter == -1) {
                cout << "Number of iterations reached the limit" << endl;
            } else {
                cout << "C-error: " << cerror << endl;
                cout << "Iters: " << iter << endl;
                cout << "Time_all (s): " << time_all << endl;
                cout << "Time_msg (s): " << time_msg << endl;
            }
            cout << endl;

            double * u; //store full numerical solution
            try {
                u = new double [M*N];
            } catch(bad_alloc &) {
                cerr << "Cant allocate memory" << endl;
                MPI_Finalize();
                return -1;
            }
            if (block.storeU(u))
                saveError(string("Error") + to_string(e) + ".txt", u, M, N, h1, h2);
            delete [] u;
        } else {//other processes
            block.storeU(nullptr);
        }
    }
    MPI_Finalize();
    return 0;
}
